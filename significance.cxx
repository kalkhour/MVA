// basic includes
#include <iostream>
#include <vector>

// root includes
#include <TString.h>

// personnal classes
#include "TMVARootTester.h"
#include "colormod.h"

void significance(TString directory)
{
  Color::Modifier def(Color::FG_DEFAULT);
  Color::Modifier red(Color::FG_RED);
  Color::Modifier blue(Color::FG_BLUE);
    
  std::cout << blue << "Testing training in directory : " << directory << def <<std::endl;
  TMVARootTester t(directory += directory.Contains("TMVA.root") ? "" :  "/TMVA.root");

  return;
}

int main( int argc, char** argv )
{
  for (int i=1; i<argc; i++)
    significance( TString(argv[i]) );

  return 0; 
}
