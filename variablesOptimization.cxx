#include <cstdio>
#include <cstdlib>
#include <memory>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <thread>       // std::this_thread::sleep_for
#include <chrono>       // std::chrono::seconds
#include <time.h>       /* time_t, time, ctime */

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TString.h"
#include "TObjString.h"

#include "TH1F.h"
#include "TGraphErrors.h"

#include <TSystem.h>
#include <TROOT.h>
#include <TStopwatch.h>

#include "TMVARootTester.h"
#include "TMVARootFoldTester.h"

using namespace std;

#include <string>

struct job { TString file;
             TString repertory;
};

std::vector<TString> GetTokens(TString string, TString delim = ":");
job writeTrainJob(TString dataset, TString sigChainList, TString bkgChainList, TString varChainListToTest, TString var3jChainListToTest, TString specVarList);


std::string exec(const char *cmd)
{
   char                  buffer[128];

   std::string           result = "";
   std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
   if (!pipe)
   {
      throw std::runtime_error("popen() failed!");
   }
   while (!feof(pipe.get()))
   {
      if (fgets(buffer, 128, pipe.get()) != NULL)
      {
         result += buffer;
      }
   }
   return(result);
}


void variablesOptimization(TString dataset, TString sigChainList, TString bkgChainList, TString varChainListToTest, TString var3jChainListToTest)
{
   TStopwatch watch;

   watch.Start();

   // Retrieve parameters in a better format
   vector<TString> sigList         = GetTokens(sigChainList);
   vector<TString> bkgList         = GetTokens(bkgChainList);
   vector<TString> varListToTest   = GetTokens(varChainListToTest);
   vector<TString> var3jListToTest = GetTokens(var3jChainListToTest);

   // baseline variables list
   TString varList = "mBBCorr:MET:HT:dPhiMETdijet:pTB1Corr:pTB2Corr:dRBBCorr:dEtaBBCorr";

   int     nJobsMax = 300;

   // BDT temoin : mBB:dRBB only
   // execute this with varList = "mBB" and varListToTest = dRBB :)


   TH1F         *rankingPlot  = new TH1F("ranking", "ranking", 300, 0, 300);
   TGraphErrors *rankingFold0 = new TGraphErrors(varListToTest.size() * (varListToTest.size() + 1) / 2 + 5); // number of points != number of bins, carefull for labels
   TGraphErrors *rankingFold1 = new TGraphErrors(varListToTest.size() * (varListToTest.size() + 1) / 2 + 5); // number of points != number of bins, carefull for labels
   rankingFold0->SetLineColor(kBlue);
   rankingFold1->SetLineColor(kRed);
   rankingFold0->SetMarkerStyle(3);
   rankingFold1->SetMarkerStyle(3);

   rankingPlot->SetMarkerStyle(8);
   int bin = 1;

   // reference training : baseline variables only
   /*
    * job thisjob_base =  writeTrainJob(dataset, sigChainList, bkgChainList, varList, var3jChainListToTest, "");// , var3jChainListToTest, specVarList);
    * if(thisjob_base.file == "" or thisjob_base.repertory == "")
    * return;
    * TString jobFileName_base = thisjob_base.file;
    *
    * gSystem->Exec("echo " + jobFileName_base);
    * gSystem->Exec("chmod ugo+x " + jobFileName_base);
    * gSystem->Exec(jobFileName_base);
    *
    * while(  TString( exec("qstat") ).ReplaceAll(" ", "").Sizeof() > 300) // 1 if not batch
    * {
    *  std::cout << "Still waiting for unfinished jobs ..." << std::endl;
    *  gSystem->Exec("qstat");
    *  std::this_thread::sleep_for (std::chrono::seconds(60));
    * }
    *
    * TMVARootTester tester_base(thisjob_base.repertory + "/" + "TMVA.root", varList);
    * rankingPlot->SetBinContent(bin, tester_base.Sensitivity());
    * rankingPlot->SetBinError(bin, 0);
    * rankingPlot->GetXaxis()->SetBinLabel(bin, varList);
    * bin += 2;
    */

   // start looping over test variables
   // 2 jets only
   while ((int)varListToTest.size() > 0)
   {
      std::map<TString, TString> jobReps;

      for (auto var:varListToTest)
      {
         if (nJobsMax < 0)
         {
            std::cout << "Reached the maximal allowed number of jobs to be automatically launched : don't panic, I will exit RIGHT NOW !!";
            std::cout << "AhAhAhahahhhh ... :p --> ||" << std::endl;
            return;
         }
         else
         {
            TString testVarList = varList + ":" + var;
            std::cout << "Testing variables " << testVarList << std::endl;
            // TString specVarList;
            // for(auto varSpec:varListToTest)
            //    {
            //      if(varSpec != var)
            //        {
            //          specVarList += varSpec;
            //          specVarList += ":";
            //        }
            //      if(specVarList.EndsWith(":")) specVarList.Remove(specVarList.Length()-1) ;
            //      else { std::cout << "anormal spectator list" << std::endl; throw; }
            //    }


            job thisjob = writeTrainJob(dataset, sigChainList, bkgChainList, testVarList, var3jChainListToTest, "");   // , var3jChainListToTest, specVarList);
            if (thisjob.file == "" or thisjob.repertory == "")
            {
               return;
            }
            TString jobFileName = thisjob.file;
            jobReps.emplace(var, thisjob.repertory);

            gSystem->Exec("echo " + jobFileName);
            gSystem->Exec("chmod ugo+x " + jobFileName);
            nJobsMax--;
            if (nJobsMax < 0)
            {
               throw;
            }
            else
            {
               std::cout << "Waiting for job " << jobFileName << std::endl;
               gSystem->Exec(jobFileName);
            }
            std::this_thread::sleep_for(std::chrono::seconds(5));    // different time is better if bad name handling
         }
      }

      // now, wait for the end of the trainings
      while (TString(exec("qstat")).ReplaceAll(" ", "").Sizeof() > 300)    // 1 if not batch
      {
         std::cout << "Still waiting for unfinished jobs ..." << std::endl;
         gSystem->Exec("qstat");
         std::this_thread::sleep_for(std::chrono::seconds(60));
      }

      // pick the best training
      map<TString, Double_t> rankValues;
      Double_t               bestSensitivity = -1.;
      int                    ibestVariable = -1, f = 0;
      for (auto var:varListToTest)
      {
         std::cout << "Build a TMVARootTester for file " << jobReps.at(var) << "/" << "TMVA.root" << std::endl;
         TMVARootTester tester(jobReps.at(var) + "/" + "TMVA.root", TString(varList).ReplaceAll(":", "_") + "_" + var);
         ibestVariable   = (tester.Sensitivity() > bestSensitivity) ? f : ibestVariable;
         bestSensitivity = (tester.Sensitivity() > bestSensitivity) ? tester.Sensitivity() : bestSensitivity;

         rankValues.emplace(var, tester.Sensitivity());

         f++;
      }

      // ranking plot
      while (rankValues.size() > 0)
      {
         double  minSensitivity = 999.;
         TString minVariable;
         for (auto it:rankValues)
         {
            if (it.second < minSensitivity)
            {
               minSensitivity = it.second;
               minVariable    = it.first;
            }
         }
         TMVARootTester tester(jobReps.at(minVariable) + "/" + "TMVA.root", TString(varList).ReplaceAll(":", "_") + "_" + minVariable);
         rankingPlot->SetBinContent(bin, minSensitivity);
         rankingPlot->SetBinError(bin, tester.Uncertainty());
         rankingPlot->GetXaxis()->SetBinLabel(bin, minVariable);

         // 2 folds
         TMVARootFoldTester   foldtester(jobReps.at(minVariable) + "/" + "TMVA.root", TString(varList).ReplaceAll(":", "_") + "_" + minVariable);
         std::vector<Float_t> sensitivityFolds = foldtester.Sensitivities();
         std::vector<Float_t> uncertaintyFolds = foldtester.Uncertainties();
         // if (sensitivityFolds.at(0) > sensitivityFolds.at(1))
         //   {
         //     std::swap(sensitivityFolds.at(0), sensitivityFolds.at(1));
         //     std::swap(uncertaintyFolds.at(0), uncertaintyFolds.at(1));
         //   }


         rankingFold0->SetPoint(bin, bin, sensitivityFolds.at(0));
         rankingFold0->SetPointError(bin, 0., uncertaintyFolds.at(0));

         rankingFold1->SetPoint(bin, bin + 0.2, sensitivityFolds.at(1));
         rankingFold1->SetPointError(bin, 0., uncertaintyFolds.at(1));


         rankValues.erase(rankValues.find(minVariable));

         bin++;
      }

      // remove largely correlated variables also
      TString removeVar = "none";
      if (varListToTest.at(ibestVariable) == "dRB1J3")
      {
         removeVar = "mindRBJ3";
      }
      if (varListToTest.at(ibestVariable) == "mindRBJ3")
      {
         removeVar = "dRB1J3";
      }
      if (varListToTest.at(ibestVariable) == "dRB2J3")
      {
         removeVar = "maxdRBJ3";
      }
      if (varListToTest.at(ibestVariable) == "maxdRBJ3")
      {
         removeVar = "dRB2J3";
      }

      varList += ":" + varListToTest.at(ibestVariable);
      varListToTest.erase(varListToTest.begin() + ibestVariable);

      for (int varToRemove = 0; varToRemove < varListToTest.size(); varToRemove++)
      {
         if (varListToTest.at(varToRemove) == removeVar)
         {
            varListToTest.erase(varListToTest.begin() + varToRemove);
         }
      }

      bin += 2;

      rankValues.clear();
      jobReps.clear();
   }

   TFile *rankingFile = new TFile("ranking.root", "RECREATE");
   rankingPlot->Write("ranking");
   rankingFold0->Write("rankingFold0");
   rankingFold1->Write("rankingFold1");
   rankingFile->Close();

   std::cout << "Best set of variables is " << varList.Data() << std::endl;

   std::cout << "\n==> Running time - ";
   watch.Stop();
   watch.Print();
   std::cout << std::endl;
} // variablesOptimization


job writeTrainJob(TString dataset, TString sigList, TString bkgList, TString varListToTest, TString var3jListToTest, TString specVarList)
{
   time_t rawtime;

   time(&rawtime);

   TString ourTime(ctime(&rawtime));
   ourTime.ReplaceAll(" ", "_").ReplaceAll(":", "_");

   TString ofsName = ourTime + "_";
   ofsName += dataset + "_";
   // ofsName += sigList + "_";
   ofsName += varListToTest + "_";
   // ofsName += var3jListToTest;

   ofsName.ReplaceAll("\n", "");
   ofsName.ReplaceAll(":", "_");

   std::ifstream ifs;
   ifs.open(TString("/sps/atlas/d/delporte/MVA/trainings/qJobTrain_") + ofsName.Data() + ".txt", std::ifstream::in);
   if (ifs.good())
   {
      std::cout << "Job file already exists : should not happen --> leaving" << std::endl;
      throw;
      return(job()); // just in case
   }
   ifs.close();

   std::fstream ofs;
   ofs.open(TString("/sps/atlas/d/delporte/MVA/trainings/qJobTrain_") + ofsName.Data() + ".txt", std::fstream::out);

   ofs << "#!/usr/local/bin/tcsh" << std::endl;
   ofs << "#" << std::endl;
   ofs << "setenv OUTPUTDIR /sps/atlas/d/delporte/MVA/trainings/LOG" << std::endl;
   ofs << "#" << std::endl;
   ofs << std::endl;
   ofs << "setenv INPUT       " << dataset << std::endl;
   ofs << "setenv VARIABLES   " << varListToTest << std::endl;
   ofs << "setenv VARIABLES3J " << var3jListToTest << std::endl;
   // ofs << "setenv VARIABLESSPEC " << specVarList                     << std::endl;
   ofs << std::endl;
   ofs << "setenv SIGNAL      " << sigList << std::endl;
   ofs << "setenv BACKGROUND  " << bkgList << std::endl;
   ofs << "setenv COMMENT     " << ofsName << std::endl;
   ofs << "echo sending job ${INPUT}  ${COMMENT}" << std::endl;
   // ofs << "qsub -P P_atlas -o $OUTPUTDIR/tmva${INPUT}_${COMMENT}.log -j y -l sps=1,ct=10000,vmem=15G,fsize=1G -N j${INPUT} -m e -M delporte@lal.in2p3.fr -V sub_Train" << std::endl;
   ofs << "qsub -P P_atlas -o $OUTPUTDIR/tmva${INPUT}_${COMMENT}.log -j y -l sps=1, -q huge -N j${INPUT} -m e -M delporte@lal.in2p3.fr -V sub_Train" << std::endl;

   ofs.close();

   job thisjob;
   thisjob.file      = TString("/sps/atlas/d/delporte/MVA/trainings/qJobTrain_") + ofsName.Data() + ".txt";
   thisjob.repertory = TString("/sps/atlas/d/delporte/MVA/trainings/TRAIN_") + dataset + "_" + ofsName;

   return(thisjob);
} // writeTrainJob


std::vector<TString> GetTokens(TString string, TString delim)
{
   TObjArray       *arrayList = string.Tokenize(delim);

   vector<TString> list;
   for (Int_t i = 0; i < arrayList->GetEntries(); i++)
   {
      list.push_back(((TObjString *)(arrayList->At(i)))->String());
   }

   delete arrayList;
   arrayList = NULL;

   return(list);
}


int main(int argc, char **argv)
{
   TString dataset(argv[1]);
   TString sigList(argv[2]);
   TString bkgList(argv[3]);
   TString varList(argv[4]);
   TString varList3j(argv[5]);

   variablesOptimization(dataset, sigList, bkgList, varList, varList3j);

   return(0);
}
